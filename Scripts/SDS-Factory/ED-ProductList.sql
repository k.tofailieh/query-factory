with products (prodm_id,regul_id,regname,reg_tp,tp,m_ref,marguage) as (
select fiche.m_id as prodm_id,reg.reg_id regul_id,reg.name regname,reg.reg_tp,fiche.tp,fiche.m_ref,fiche.marguage
from reg
    INNER JOIN ENTR_reg_regentry reg_regentry_REGULATION_ENTRY ON reg_regentry_REGULATION_ENTRY.entr_src_id = reg.reg_id AND reg_regentry_REGULATION_ENTRY.entr_tp LIKE 'REGULATION_ENTRY'
    INNER JOIN ENTR_regentry_substances regentry_substances_SUBSTANCE_CONCERNED ON regentry_substances_SUBSTANCE_CONCERNED.entr_src_id = reg_regentry_REGULATION_ENTRY.entr_dst_id     AND regentry_substances_SUBSTANCE_CONCERNED.entr_tp LIKE 'SUBSTANCE_CONCERNED'
    INNER JOIN ENTR_nomclit_substances substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R ON substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_dst_id = regentry_substances_SUBSTANCE_CONCERNED.entr_dst_id AND substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_tp IN ('NOMENCLATURE_ITEM_FOR_MATERIAL_IS','NOMENCLATURE_ITEM_FOR_RAW_MATERIAL_IS','NOMENCLATURE_ITEM_FOR_PRODUCT_IS')
    INNER JOIN ENTR_fiche_nomclit nomclit_fiche_REAL_COMPO_SUBSTANCE_R ON nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_dst_id = substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_src_id AND nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_tp IN ('REAL_COMPO_SUBSTANCE')
INNER JOIN fiche fiche ON fiche.m_id=nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_src_id
WHERE reg.reg_id = 1060 and fiche.tp in ('MAT','PRODUCT','RAW_MATERIAL')
GROUP BY fiche.m_id,reg.reg_id,reg.name,reg.reg_tp,fiche.tp,fiche.m_ref,fiche.marguage
UNION
select fiche.m_id,reg.reg_id,reg.name,reg.reg_tp,fiche.tp,fiche.m_ref,fiche.marguage
from reg
    INNER JOIN ENTR_reg_regentry reg_regentry_REGULATION_ENTRY ON reg_regentry_REGULATION_ENTRY.entr_src_id = reg.reg_id AND reg_regentry_REGULATION_ENTRY.entr_tp LIKE 'REGULATION_ENTRY'
    INNER JOIN ENTR_regentry_subset regentry_subset_SUBSET_CONCERNED ON regentry_subset_SUBSET_CONCERNED.entr_src_id = reg_regentry_REGULATION_ENTRY.entr_dst_id AND regentry_subset_SUBSET_CONCERNED.entr_tp LIKE 'SUBSET_CONCERNED'
    INNER JOIN ENTR_subset_substances subset_substances_IS_IN ON subset_substances_IS_IN.entr_src_id = regentry_subset_SUBSET_CONCERNED.entr_dst_id AND subset_substances_IS_IN.entr_tp LIKE 'IS_IN'
    INNER JOIN ENTR_nomclit_substances substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R ON substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_dst_id = subset_substances_IS_IN.entr_dst_id AND substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_tp IN ('NOMENCLATURE_ITEM_FOR_MATERIAL_IS','NOMENCLATURE_ITEM_FOR_RAW_MATERIAL_IS','NOMENCLATURE_ITEM_FOR_PRODUCT_IS')
    INNER JOIN ENTR_fiche_nomclit nomclit_fiche_REAL_COMPO_SUBSTANCE_R ON nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_dst_id = substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_src_id AND nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_tp IN ('REAL_COMPO_SUBSTANCE')
INNER JOIN fiche fiche ON fiche.m_id=nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_src_id
WHERE reg.reg_id = 1060 and fiche.tp in ('MAT','PRODUCT','RAW_MATERIAL') and coalesce(nullif(fiche.ns, ''), '*') = :ORG
GROUP BY fiche.m_id,reg.reg_id,reg.name,reg.reg_tp,fiche.tp,fiche.m_ref,fiche.marguage
ORDER BY m_ref,marguage)
SELECT distinct products.prodm_id, products.m_ref prodname, products.marguage, nomclit_id, sub_id, subset_id, regentry_id, reg_id, regname,
substances.cas AS cas, substances.ec AS ec, substances.naml AS naml,
nomclit.conc_direct_lbound_inc, nomclit.conc_direct_lbound, nomclit.conc_direct_ubound, nomclit.conc_direct_ubound_inc, nomclit.conc_unit,
regentry_ref, regentry_tp,conc_outofrange_meaning,conc_threshold_lbound_inc,conc_threshold_lbound,conc_threshold_ubound,conc_threshold_ubound_inc, regulatedNomclit.conc_unit, assertion1, assertion2, assertion3, assertion4
FROM fiche
INNER JOIN products ON  fiche.m_id = products.prodm_id
    INNER JOIN ENTR_fiche_nomclit fiche_nomclit_REAL_COMPO_SUBSTANCE ON fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_src_id = fiche.m_id AND fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_tp LIKE 'REAL_COMPO_SUBSTANCE'
    INNER JOIN nomclit nomclit ON nomclit.nomclit_id=fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_dst_id
    INNER JOIN ENTR_nomclit_substances nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS ON nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_src_id = nomclit.nomclit_id AND nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_tp IN ('NOMENCLATURE_ITEM_FOR_MATERIAL_IS','NOMENCLATURE_ITEM_FOR_RAW_MATERIAL_IS','NOMENCLATURE_ITEM_FOR_PRODUCT_IS')
    INNER JOIN substances substances ON substances.sub_id=nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_dst_id
  LEFT OUTER JOIN (
  SELECT substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_src_id AS nomclit_id, null AS subset_id,regentry_id,reg_id,
  regentry.ref as regentry_ref,regentry.tp as regentry_tp,conc_outofrange_meaning,conc_threshold_lbound_inc,conc_threshold_lbound,conc_threshold_ubound,conc_threshold_ubound_inc, conc_unit, assertion1, assertion2, assertion3, assertion4
  FROM reg
      INNER JOIN ENTR_reg_regentry reg_regentry_REGULATION_ENTRY ON reg_regentry_REGULATION_ENTRY.entr_src_id = reg.reg_id AND reg_regentry_REGULATION_ENTRY.entr_tp LIKE 'REGULATION_ENTRY'
      INNER JOIN regentry regentry ON regentry.regentry_id=reg_regentry_REGULATION_ENTRY.entr_dst_id
      INNER JOIN ENTR_regentry_substances regentry_substances_SUBSTANCE_CONCERNED ON regentry_substances_SUBSTANCE_CONCERNED.entr_src_id = reg_regentry_REGULATION_ENTRY.entr_dst_id     AND regentry_substances_SUBSTANCE_CONCERNED.entr_tp LIKE 'SUBSTANCE_CONCERNED'
      INNER JOIN ENTR_nomclit_substances substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R ON substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_dst_id = regentry_substances_SUBSTANCE_CONCERNED.entr_dst_id AND substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_tp IN ('NOMENCLATURE_ITEM_FOR_MATERIAL_IS','NOMENCLATURE_ITEM_FOR_RAW_MATERIAL_IS','NOMENCLATURE_ITEM_FOR_PRODUCT_IS')
      INNER JOIN ENTR_fiche_nomclit nomclit_fiche_REAL_COMPO_SUBSTANCE_R ON nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_dst_id = substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_src_id AND nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_tp IN ('REAL_COMPO_SUBSTANCE')
      WHERE reg.reg_id = 1060
  UNION ALL
  SELECT substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_src_id AS nomclit_id, regentry_subset_SUBSET_CONCERNED.entr_dst_id AS subset_id,regentry_id,reg_id,
  regentry.ref as regentry_ref,regentry.tp as regentry_tp,conc_outofrange_meaning,conc_threshold_lbound_inc,conc_threshold_lbound,conc_threshold_ubound,conc_threshold_ubound_inc, conc_unit, assertion1, assertion2, assertion3, assertion4
  FROM reg
    INNER JOIN ENTR_reg_regentry reg_regentry_REGULATION_ENTRY ON reg_regentry_REGULATION_ENTRY.entr_src_id = reg.reg_id AND reg_regentry_REGULATION_ENTRY.entr_tp LIKE 'REGULATION_ENTRY'
    INNER JOIN regentry regentry ON regentry.regentry_id=reg_regentry_REGULATION_ENTRY.entr_dst_id
    INNER JOIN ENTR_regentry_subset regentry_subset_SUBSET_CONCERNED ON regentry_subset_SUBSET_CONCERNED.entr_src_id = reg_regentry_REGULATION_ENTRY.entr_dst_id AND regentry_subset_SUBSET_CONCERNED.entr_tp LIKE 'SUBSET_CONCERNED'
    INNER JOIN ENTR_subset_substances subset_substances_IS_IN ON subset_substances_IS_IN.entr_src_id = regentry_subset_SUBSET_CONCERNED.entr_dst_id AND subset_substances_IS_IN.entr_tp LIKE 'IS_IN'
    INNER JOIN ENTR_nomclit_substances substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R ON substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_dst_id = subset_substances_IS_IN.entr_dst_id AND substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_tp IN ('NOMENCLATURE_ITEM_FOR_MATERIAL_IS','NOMENCLATURE_ITEM_FOR_RAW_MATERIAL_IS','NOMENCLATURE_ITEM_FOR_PRODUCT_IS')
    INNER JOIN ENTR_fiche_nomclit nomclit_fiche_REAL_COMPO_SUBSTANCE_R ON nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_dst_id = substances_nomclit_NOMENCLATURE_ITEM_FOR_MATERIAL_IS_R.entr_src_id AND nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_tp IN ('REAL_COMPO_SUBSTANCE')
    INNER JOIN products ON reg.reg_id = products.regul_id   AND nomclit_fiche_REAL_COMPO_SUBSTANCE_R.entr_src_id = products.prodm_id
    WHERE reg.reg_id = 1060)
    regulatedNomclit USING(nomclit_id)
;
