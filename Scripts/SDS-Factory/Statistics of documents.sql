select doc_id, doc_file_name, doc_cr_dt, md_dt, doc_path, doc_type, authors, tp, tptp, doc_version, lg, validation_dt, ns,
(select string_agg(docr_tp, ',') from docr_dir where docr_dst_id = doc_id) pers_or_soc,
(select string_agg(docr_tp, ',') from docr_fiche where docr_dst_id = doc_id) products,
(select string_agg(docr_tp, ',') from docr_sds where docr_dst_id = doc_id) sds,
(select string_agg(docr_tp, ',') from docr_doc where docr_dst_id = doc_id) docs,
(select string_agg(de.de_li, ',') from docr_sds inner join sdsdoc on docr_sds.docr_src_id = sdsdoc.sdsdoc_id inner join der_sdsdoc on der_sdsdoc.der_src_id = sdsdoc.sdsdoc_id inner join de on der_sdsdoc.der_dst_id = de.de_id where docr_sds.docr_dst_id = doc.doc_id) rr
from doc
where coalesce(nullif(ns, ''), '*') = :user_organisation
and coalesce(doc_type,'') <> 'sdsV2/xml'
order by 14,15,16,17
