WITH
product as (
SELECT m_id, m_ref, marguage,
 supplier_name,
 site_division,
 string_agg(DISTINCT site_li,', ' ORDER BY site_li) as sites,
 clps, clpsh,
 /*ENDPOINT DATA*/
 point_eclair_operator,
 point_eclair_value,
 point_ebullition_operator,
 point_ebullition_value,
 ph,
 cinematic_viscosity,
 cov,
 vapour_pressure,
 formol_liberation
FROM (
SELECT m_id, m_ref, marguage,
  /* Fournisseur */
  SUPPLIER.displayname as supplier_name,
  /* Site */
  (select site_division.site_li from site site_division where
    site.site_lft between site_division.site_lft+1 and site_division.site_rgt-1 AND
    site.site_rgt between site_division.site_lft+1 and site_division.site_rgt-1 AND
    site_division.tree_key = site.tree_key AND
    site_division.site_st = 'VALID' AND
    site_division.site_tp <> 'siege' AND
    site_division.site_lft >1
    ORDER by site_division.site_lft ASC
    LIMIT 1
  ) as site_division,
  site.site_li,
  /* CLP */
  lCLP.clps,
  /* CLP-Hnnn */
  lCLP.clpsh,
  /*ENDPOINT DATA*/
  ventr_fiche_flashpoint.value_operator as point_eclair_operator,
  ventr_fiche_flashpoint.val as point_eclair_value,
  ventr_fiche_boilingpoint.value_operator as point_ebullition_operator,
  ventr_fiche_boilingpoint.val as point_ebullition_value,
  ventr_fiche_ph.val as ph,
  ventr_fiche_cinematic_viscosity.val as cinematic_viscosity,
  ventr_fiche_cov.val as cov,
  ventr_fiche_vapour_pressure.val as vapour_pressure,
  ventr_fiche_formol_liberation.val as formol_liberation
FROM fiche
/* Fournisseur */
LEFT OUTER JOIN entr_fiche_dir lSUPPLIER ON lSUPPLIER.entr_src_id = m_id AND lSUPPLIER.entr_tp = 'SUPPLIER'
LEFT OUTER JOIN dir SUPPLIER on supplier.dir_id = lSUPPLIER.entr_dst_id
/* Site */
LEFT OUTER JOIN entr_presence_fiche AS epf ON epf.entr_dst_id = m_id AND epf.entr_tp = 'MATERIAL'
LEFT OUTER JOIN entr_site_presence AS esp ON esp.entr_dst_id = epf.entr_src_id AND esp.entr_tp = 'MATERIAL'
LEFT OUTER JOIN site ON site_id = esp.entr_src_id
/* CLP */
LEFT OUTER JOIN
    (SELECT efp.entr_src_id, string_agg(DISTINCT HClassCat.plcsys_ke, ', ' ORDER BY HClassCat.plcsys_ke) AS clps, string_agg(DISTINCT plcsys.plcsys_ke, ', ' ORDER BY plcsys.plcsys_ke) AS clpsh
    FROM entr_fiche_plc efp
    INNER JOIN plc on efp.entr_dst_id = plc.plc_id
    INNER JOIN entr_plc_plcsys epps ON plc.plc_id = epps.entr_src_id and epps.entr_tp in ('HAZARD_CLASS_CAT')
    INNER JOIN plcsys HClassCat on HClassCat.plcsys_id = epps.entr_dst_id
    INNER JOIN entr_plcsys_plcsys epsps ON epsps.entr_src_id = epps.entr_dst_id AND epsps.entr_tp = concat('GHS_H_STMT_', :REGULATORY_REFERENTIAL)
    INNER JOIN plcsys on plcsys.plcsys_id = epsps.entr_dst_id AND plcsys.name = 'GHS_H_STMT'
    WHERE efp.entr_tp = concat('CLP_S_', :REGULATORY_REFERENTIAL)
    GROUP BY efp.entr_src_id) lCLP on lCLP.entr_src_id = m_id
 /*FLASHPOINT*/
 LEFT OUTER JOIN (SELECT * FROM entr_fiche_endpt as efe
    INNER JOIN endpt on efe.entr_dst_id = endpt_id
    INNER JOIN der_endpt on der_endpt.der_src_id = endpt_id
    INNER JOIN de on der_endpt.der_dst_id = de_id
   WHERE de_ke = 'POINT_ECLAIR' ) ventr_fiche_flashpoint on ventr_fiche_flashpoint.entr_src_id = m_id and ventr_fiche_flashpoint.entr_tp = 'MAT_ENDPT'
 /*BOILINGPOINT*/
 LEFT OUTER JOIN (SELECT * FROM entr_fiche_endpt as efe
    INNER JOIN endpt on efe.entr_dst_id = endpt_id
    INNER JOIN der_endpt on der_endpt.der_src_id = endpt_id
    INNER JOIN de on der_endpt.der_dst_id = de_id
   WHERE de_ke = 'BOILPOINT' ) ventr_fiche_boilingpoint on ventr_fiche_boilingpoint.entr_src_id = m_id and ventr_fiche_boilingpoint.entr_tp = 'MAT_ENDPT'
 /*VAPOUR PRESSURE*/
 LEFT OUTER JOIN (SELECT * FROM entr_fiche_endpt as efe
    INNER JOIN endpt on efe.entr_dst_id = endpt_id
    INNER JOIN der_endpt on der_endpt.der_src_id = endpt_id
    INNER JOIN de on der_endpt.der_dst_id = de_id
   WHERE de_ke = 'VAPOUR_PRESSURE' ) ventr_fiche_vapour_pressure on ventr_fiche_vapour_pressure.entr_src_id = m_id and ventr_fiche_vapour_pressure.entr_tp = 'MAT_ENDPT'
 /*CINEMATIC VISCOSITY*/
 LEFT OUTER JOIN (SELECT * FROM entr_fiche_endpt as efe
    INNER JOIN endpt on efe.entr_dst_id = endpt_id
    INNER JOIN der_endpt on der_endpt.der_src_id = endpt_id
    INNER JOIN de on der_endpt.der_dst_id = de_id
   WHERE de_ke = 'CINEMATIC_VISCOSITY' ) ventr_fiche_cinematic_viscosity on ventr_fiche_cinematic_viscosity.entr_src_id = m_id and ventr_fiche_cinematic_viscosity.entr_tp = 'MAT_ENDPT'
 /*PH*/
 LEFT OUTER JOIN (SELECT * FROM entr_fiche_endpt as efe
    INNER JOIN endpt on efe.entr_dst_id = endpt_id
    INNER JOIN der_endpt on der_endpt.der_src_id = endpt_id
    INNER JOIN de on der_endpt.der_dst_id = de_id
   WHERE de_ke = 'PH' ) ventr_fiche_ph on ventr_fiche_ph.entr_src_id = m_id and ventr_fiche_ph.entr_tp = 'MAT_ENDPT'
 /*COV*/
 LEFT OUTER JOIN (SELECT * FROM entr_fiche_endpt as efe
    INNER JOIN endpt on efe.entr_dst_id = endpt_id
    INNER JOIN der_endpt on der_endpt.der_src_id = endpt_id
    INNER JOIN de on der_endpt.der_dst_id = de_id
   WHERE de_ke = 'COV' ) ventr_fiche_cov on ventr_fiche_cov.entr_src_id = m_id and ventr_fiche_cov.entr_tp = 'MAT_ENDPT'
 /*FORMOL_LIBERATION*/
 LEFT OUTER JOIN (SELECT * FROM entr_fiche_endpt as efe
    INNER JOIN endpt on efe.entr_dst_id = endpt_id
    INNER JOIN der_endpt on der_endpt.der_src_id = endpt_id
    INNER JOIN de on der_endpt.der_dst_id = de_id
   WHERE de_ke = 'FORMOL_LIBERATION' ) ventr_fiche_formol_liberation on ventr_fiche_formol_liberation.entr_src_id = m_id and ventr_fiche_formol_liberation.entr_tp = 'MAT_ENDPT'
WHERE COALESCE(fiche.active,0) <> 0 AND coalesce(fiche.ns, '*') = :user_organisation
) t
GROUP BY m_id, m_ref, marguage, supplier_name, site_division, clps, clpsh, point_eclair_operator, point_eclair_value, point_ebullition_operator, point_ebullition_value, ph, cinematic_viscosity, cov, vapour_pressure, formol_liberation
),
vlep AS (
    SELECT m_id AS product_id, sub_id, MIN(ers.entr_float2) AS min_vle, COUNT(reg.reg_id) AS nb_country, STRING_AGG(DISTINCT de_ke, ', ' ORDER BY de_ke) AS countries
    FROM fiche
    INNER JOIN entr_fiche_nomclit efn ON efn.entr_src_id = m_id AND efn.entr_tp = 'REAL_COMPO_SUBSTANCE'
    INNER JOIN entr_nomclit_substances ens ON ens.entr_src_id = efn.entr_dst_id AND ens.entr_tp =  'NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
    INNER JOIN substances s ON s.sub_id = ens.entr_dst_id
    INNER JOIN entr_reg_substances ers ON ers.entr_src_id = s.sub_id
    INNER JOIN reg ON reg.reg_id = ers.entr_dst_id AND reg.reg_ke LIKE 'OCC_EXPOSURE_LIMITS%'
    INNER JOIN der_reg ON der_src_id = reg_id AND der_tp ='REGULATION_COUNTRY'
    INNER JOIN de ON de_id = der_dst_id
    WHERE COALESCE(fiche.active,0) <> 0 AND  coalesce(fiche.ns, '*') = :user_organisation
    GROUP BY m_id, sub_id
)
SELECT m_id, m_ref, marguage,
  /* Fournisseur */
  supplier_name,
  /* Site */
  site_division,
  sites,
  /* CLP */
  product.clps,
  /* CLP - Hnnn */
  product.clpsh,
  /*ENDPOINT DATA*/
  point_eclair_operator,
  point_eclair_value,
  point_ebullition_operator,
  point_ebullition_value,
  ph,
  cinematic_viscosity,
  cov,
  vapour_pressure,
  formol_liberation,
  /* composition */
MAX(composition.conc_real_ubound) as concentration_max,
  substances.cas, substances.ec, substances.idx, substances.naml,
  registred_sub.entr_id is not null as isregistered,
  svhc_sub.entr_id is not null as issvhc,
  exempted_sub.entr_id is not null as isexempted,
  (COALESCE(substances.cas,'') = '50-00-0' AND COALESCE(composition.conc_real_ubound, 100) > 0.1) as isformaldehyde01,
  (
    EXISTS (SELECT 1
            FROM entr_nomclit_plc
            INNER JOIN entr_plc_plcsys on entr_plc_plcsys.entr_src_id = entr_nomclit_plc.entr_dst_id and entr_plc_plcsys.entr_tp in ('HAZARD_CLASS_CAT','RISK')
            INNER JOIN plcsys on plcsys.plcsys_id = entr_plc_plcsys.entr_dst_id
            WHERE entr_nomclit_plc.entr_src_id = nomclit_id and entr_nomclit_plc.entr_tp = concat('ITEM_CLASSIFICATION_SELECTION_', :REGULATORY_REFERENTIAL)
            AND plcsys.plcsys_ke similar to '(Muta. 1|Carc. 1|Repr. 1|R45|R46|R49|R60|R61)%'
            )
  ) as cmr1,
  (
    EXISTS (SELECT 1
            FROM entr_nomclit_plc
            INNER JOIN entr_plc_plcsys on entr_plc_plcsys.entr_src_id = entr_nomclit_plc.entr_dst_id and entr_plc_plcsys.entr_tp in ('HAZARD_CLASS_CAT','RISK')
            INNER JOIN plcsys on plcsys.plcsys_id = entr_plc_plcsys.entr_dst_id
            WHERE entr_nomclit_plc.entr_src_id = nomclit_id and entr_nomclit_plc.entr_tp = concat('ITEM_CLASSIFICATION_SELECTION_', :REGULATORY_REFERENTIAL)
            AND plcsys.plcsys_ke similar to '(Muta. 2|Carc. 2|Repr. 2|R40|R62|R63|R68)%'
            )
  ) as cmr2,
    COALESCE(vlep.min_vle, 0) AS min_vle,
    COALESCE(vlep.nb_country, 0) AS nb_country,
    COALESCE(vlep.countries, '') AS countries,
    sCLP.name as substance_classification_name,
    sCLP.clps as substance_classification
FROM product
/* Composition */
LEFT OUTER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE ON ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_src_id = product.m_id and ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_tp='REAL_COMPO_SUBSTANCE'
LEFT OUTER JOIN nomclit composition on composition.nomclit_id=ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_dst_id
LEFT OUTER JOIN ENTR_nomclit_substances ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS ON ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_src_id = composition.nomclit_id and ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_tp='NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
LEFT OUTER JOIN substances on substances.sub_id=ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_dst_id
LEFT OUTER JOIN entr_subset_substances svhc_sub on svhc_sub.entr_dst_id = sub_id and svhc_sub.entr_tp = 'IS_IN' and svhc_sub.entr_src_id = (SELECT subset_id FROM subset where key = 'All SVHC')
LEFT OUTER JOIN vlep ON vlep.product_id = product.m_id AND vlep.sub_id = substances.sub_id
LEFT OUTER JOIN entr_reg_substances registred_sub on registred_sub.entr_dst_id = substances.sub_id and registred_sub.entr_tp = 'SUBSTANCES' and registred_sub.entr_src_id = 100
LEFT OUTER JOIN entr_reg_substances exempted_sub on exempted_sub.entr_dst_id = substances.sub_id and exempted_sub.entr_tp = 'SUBSTANCES' and exempted_sub.entr_src_id = 27
/* component CLP */
LEFT OUTER JOIN
    (SELECT enp.entr_src_id as sCLP_nomclit_id, plc.name, string_agg(DISTINCT plcsys.plcsys_ke, ', ' ORDER BY plcsys.plcsys_ke) AS clps
    FROM entr_nomclit_plc enp
    INNER JOIN plc on enp.entr_dst_id = plc.plc_id
    INNER JOIN entr_plc_plcsys epps ON plc.plc_id = epps.entr_src_id and epps.entr_tp in ('HAZARD_CLASS_CAT')
    INNER JOIN plcsys on plcsys.plcsys_id = epps.entr_dst_id
    WHERE enp.entr_tp = concat('ITEM_CLASSIFICATION_SELECTION_', :REGULATORY_REFERENTIAL)
    GROUP BY enp.entr_src_id, plc.name) sCLP on sCLP_nomclit_id = composition.nomclit_id
group by
m_id, m_ref, marguage,supplier_name, site_division, sites,
product.clps, product.clpsh,point_eclair_operator,point_eclair_value,point_ebullition_operator,point_ebullition_value,
ph,cinematic_viscosity,cov,vapour_pressure, formol_liberation,
substances.cas, substances.ec, substances.idx, substances.naml, isregistered, issvhc,isexempted, isformaldehyde01, cmr1, cmr2,
min_vle, nb_country, countries, substance_classification_name, substance_classification
ORDER BY  m_ref, marguage, substances.naml, substances.cas, substances.ec, concentration_max DESC
