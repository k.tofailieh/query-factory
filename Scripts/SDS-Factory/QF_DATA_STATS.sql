WITH
allMaterials AS  (
SELECT
ns,
COUNT (nullif(active,0)) AS active_fiche,
COUNT (nullif(nullif(active,1),2)) AS inactive_fiche,
MAX(sds_up_dt) AS last_modified_fiche_dt
FROM fiche GROUP BY ns ORDER BY ns ASC
),
allns AS (
SELECT DISTINCT ns FROM sdsdoc
),
allSds AS (
SELECT
allns.ns AS ns,
COALESCE(in_progress_sds.nb_sds, 0) AS sds_in_progress,
DATE(in_progress_sds.maxdate) AS in_progress_sds_dt,
COALESCE(in_progress_sds.nb_file_exist, 0) AS nb_file_exist_in_progress,
COALESCE(in_progress_sds.nb_uploaded_sds, 0) AS nb_uploaded_sds_in_progress,
COALESCE(done_sds.nb_sds, 0) AS sds_done,
DATE(done_sds.maxdate) AS done_sds_dt,
COALESCE(done_sds.nb_file_exist, 0) AS nb_file_exist_done,
COALESCE(done_sds.nb_uploaded_sds, 0) AS nb_uploaded_sds_done,
COALESCE(archived_sds.nb_sds, 0) AS archived_sds,
DATE(archived_sds.maxdate) AS archived_sds_dt,
COALESCE(archived_sds.nb_file_exist, 0) AS nb_file_exist_archived,
COALESCE(archived_sds.nb_uploaded_sds, 0) AS nb_uploaded_sds_archived,
COALESCE(valid_sds.nb_sds, 0) AS valid_sds,
DATE(valid_sds.maxdate) AS valid_sds_dt,
COALESCE(valid_sds.nb_file_exist, 0) AS nb_file_exist_valid,
COALESCE(valid_sds.nb_uploaded_sds, 0) AS nb_uploaded_sds_valid,
COALESCE(new_sds.nb_sds, 0) AS new_sds,
DATE(new_sds.maxdate) AS new_sds_dt,
COALESCE(new_sds.nb_file_exist, 0) AS nb_file_exist_new,
COALESCE(new_sds.nb_uploaded_sds, 0) AS nb_uploaded_sds_new,
COALESCE(other_sds.nb_sds, 0) AS other_sds,
DATE(other_sds.maxdate) AS other_sds_dt,
COALESCE(other_sds.nb_file_exist, 0) AS nb_file_exist_other,
COALESCE(other_sds.nb_uploaded_sds, 0) AS nb_uploaded_sds_other,
COALESCE(resume_sds.nb_sds, 0) AS resume_sds,
DATE(resume_sds.maxdate) AS resume_sds_dt,
COALESCE(resume_sds.nb_file_exist, 0) AS nb_file_exist_all,
COALESCE(resume_sds.nb_uploaded_sds, 0) AS nb_uploaded_sds_all
FROM allns
FULL OUTER JOIN (
SELECT
ns,
COUNT(1)as nb_sds,
MAX(md_dt) as maxdate,
COUNT (nullif(ds.docr_tp,'SDS_ORG')) AS nb_file_exist,
COUNT (nullif(ds.docr_tp,'SDS')) AS nb_uploaded_sds
FROM sdsdoc
LEFT JOIN docr_sds ds ON ds.docr_src_id = sdsdoc.sdsdoc_id AND ds.docr_tp IN ('SDS_ORG', 'SDS')
WHERE status IN ('INPROGRESS', 'B-IN_PROGRESS') GROUP BY ns
) AS in_progress_sds ON in_progress_sds.ns = allns.ns
FULL OUTER JOIN (
SELECT
ns,
COUNT(1)as nb_sds,
MAX(md_dt) as maxdate,
COUNT (nullif(ds.docr_tp,'SDS_ORG')) AS nb_file_exist,
COUNT (nullif(ds.docr_tp,'SDS')) AS nb_uploaded_sds
FROM sdsdoc
LEFT JOIN docr_sds ds ON ds.docr_src_id = sdsdoc.sdsdoc_id AND ds.docr_tp IN ('SDS_ORG', 'SDS')
WHERE status IN ('DONE', 'D-END_OF_LIFE') GROUP BY ns
) AS done_sds ON done_sds.ns = allns.ns
FULL OUTER JOIN (
SELECT
ns,
COUNT(1)as nb_sds,
MAX(md_dt) as maxdate,
COUNT (nullif(ds.docr_tp,'SDS_ORG')) AS nb_file_exist,
COUNT (nullif(ds.docr_tp,'SDS')) AS nb_uploaded_sds
FROM sdsdoc
LEFT JOIN docr_sds ds ON ds.docr_src_id = sdsdoc.sdsdoc_id AND ds.docr_tp IN ('SDS_ORG', 'SDS')
WHERE status IN ('ARCHIVED', 'E-ARCHIVED') GROUP BY ns
) AS archived_sds ON archived_sds.ns = allns.ns
FULL OUTER JOIN (
SELECT
ns,
COUNT(1)as nb_sds,
MAX(md_dt) as maxdate,
COUNT (nullif(ds.docr_tp,'SDS_ORG')) AS nb_file_exist,
COUNT (nullif(ds.docr_tp,'SDS')) AS nb_uploaded_sds
FROM sdsdoc
LEFT JOIN docr_sds ds ON ds.docr_src_id = sdsdoc.sdsdoc_id AND ds.docr_tp IN ('SDS_ORG', 'SDS')
WHERE status IN ('VALID', 'C-VALID') GROUP BY ns
) AS valid_sds ON valid_sds.ns = allns.ns
FULL OUTER JOIN (
SELECT
ns,
COUNT(1)as nb_sds,
MAX(md_dt) as maxdate,
COUNT (nullif(ds.docr_tp,'SDS_ORG')) AS nb_file_exist,
COUNT (nullif(ds.docr_tp,'SDS')) AS nb_uploaded_sds
FROM sdsdoc
LEFT JOIN docr_sds ds ON ds.docr_src_id = sdsdoc.sdsdoc_id AND ds.docr_tp IN ('SDS_ORG', 'SDS')
WHERE status IN ('NEW', 'A-NEW') GROUP BY ns
) AS new_sds ON new_sds.ns = allns.ns
FULL OUTER JOIN (
SELECT
ns,
COUNT(1)as nb_sds,
MAX(md_dt) as maxdate,
COUNT (nullif(ds.docr_tp,'SDS_ORG')) AS nb_file_exist,
COUNT (nullif(ds.docr_tp,'SDS')) AS nb_uploaded_sds
FROM sdsdoc
LEFT JOIN docr_sds ds ON ds.docr_src_id = sdsdoc.sdsdoc_id AND ds.docr_tp IN ('SDS_ORG', 'SDS')
WHERE status NOT IN ('INPROGRESS', 'B-IN_PROGRESS', 'DONE', 'D-END_OF_LIFE', 'ARCHIVED', 'E-ARCHIVED', 'VALID', 'C-VALID', 'NEW', 'A-NEW', 'TEMPLATE') GROUP BY ns
) AS other_sds ON other_sds.ns = allns.ns
FULL OUTER JOIN (
SELECT
ns,
COUNT(1)as nb_sds,
MAX(md_dt) as maxdate,
COUNT (nullif(ds.docr_tp,'SDS_ORG')) AS nb_file_exist,
COUNT (nullif(ds.docr_tp,'SDS')) AS nb_uploaded_sds
FROM sdsdoc
LEFT JOIN docr_sds ds ON ds.docr_src_id = sdsdoc.sdsdoc_id AND ds.docr_tp IN ('SDS_ORG', 'SDS') WHERE status NOT IN ('TEMPLATE') GROUP BY ns
) AS resume_sds ON resume_sds.ns = allns.ns
)
SELECT
COALESCE(allMaterials.ns, allSds.ns) AS ns,
COALESCE(allMaterials.active_fiche, 0) AS active_fiche,
COALESCE(allMaterials.inactive_fiche, 0) AS inactive_fiche,
allMaterials.last_modified_fiche_dt,
COALESCE(allSds.new_sds, 0) AS new_sds,
allSds.new_sds_dt,
COALESCE(allSds.nb_file_exist_new, 0) AS new_exist_file,
COALESCE(allSds.nb_uploaded_sds_new, 0) AS new_uploaded_file,
COALESCE(allSds.sds_in_progress, 0) AS sds_in_progress,
allSds.in_progress_sds_dt,
COALESCE(allSds.nb_file_exist_in_progress, 0) AS in_progress_exist_file,
COALESCE(allSds.nb_uploaded_sds_in_progress, 0) AS in_progress_uploaded_file,
COALESCE(allSds.sds_done, 0) AS sds_done,
allSds.done_sds_dt,
COALESCE(allSds.nb_file_exist_done, 0) AS done_exist_file,
COALESCE(allSds.nb_uploaded_sds_done, 0) AS done_uploaded_file,
COALESCE(allSds.archived_sds, 0) AS archived_sds,
allSds.archived_sds_dt,
COALESCE(allSds.nb_file_exist_archived, 0) AS archived_exist_file,
COALESCE(allSds.nb_uploaded_sds_archived, 0) AS archived_uploaded_file,
COALESCE(allSds.valid_sds, 0) AS valid_sds,
allSds.valid_sds_dt,
COALESCE(allSds.nb_file_exist_valid, 0) AS valid_exist_file,
COALESCE(allSds.nb_uploaded_sds_valid, 0) AS valid_uploaded_file,
COALESCE(allSds.other_sds, 0) AS other_sds,
allSds.other_sds_dt,
COALESCE(allSds.nb_file_exist_other, 0) AS other_exist_file,
COALESCE(allSds.nb_uploaded_sds_other, 0) AS other_uploaded_file,
COALESCE(allSds.resume_sds, 0) AS resume_sds,
allSds.resume_sds_dt,
COALESCE(allSds.nb_file_exist_all, 0) AS nb_exist_file,
COALESCE(allSds.nb_uploaded_sds_all, 0) AS nb_uploaded_file
FROM allMaterials
FULL OUTER JOIN allSds ON allSds.ns = allMaterials.ns ORDER BY ns ASC;





------------------------------------------------------- META 2-----------------------------------------


SELECT 'app' AS entityname, COUNT(1) AS orphans FROM app WHERE app_id IN (
SELECT app_id FROM app
EXCEPT SELECT entr_dst_id FROM entr_fiche_app
EXCEPT SELECT entr_dst_id FROM anx.entr_inci_app
EXCEPT SELECT entr_dst_id FROM entr_profile_app
)
UNION ALL
SELECT 'inforeq' AS entityname, COUNT(1) AS orphans FROM inforeq WHERE inforeq_id IN (
SELECT inforeq_id FROM inforeq EXCEPT SELECT entr_dst_id FROM entr_inforeqset_inforeq
)
UNION ALL
SELECT 'nomclit' AS entityname, COUNT(1) AS orphans from nomclit where nomclit_id in (
select nomclit_id from nomclit
EXCEPT select entr_dst_id from entr_fiche_nomclit
EXCEPT select entr_dst_id from entr_part_nomclit
EXCEPT select entr_dst_id from entr_profile_nomclit
)
UNION ALL
SELECT 'presence' AS entityname, COUNT(1) AS orphans from presence where presence_id not in (
select entr_dst_id from entr_site_presence
)
UNION ALL

SELECT 'sdsdoc' AS entityname, COUNT(1) AS orphans  from sdsdoc where sdsdoc_id IN (
SELECT sdsdoc_id FROM sdsdoc
EXCEPT SELECT entr_dst_id FROM entr_fiche_sdsdoc
) AND COALESCE(sdsdoc.tp,'') NOT IN ('TEMPLATE', 'TEMPLATE_PIF')
UNION ALL
SELECT 'fiche' AS entityname, COUNT(1) AS orphans  from fiche where (fiche.tp like 'SDSCHAPS_%' or fiche.tp like 'SNAPSHOT_%')
UNION ALL
SELECT 'sdselt' AS entityname, COUNT(1) AS orphans from sdselt where sdselt_id not in (
select entr_dst_id from entr_sdschap_sdselt
)
UNION ALL
SELECT 'phractxtke' AS entityname, COUNT(1) AS orphans from phractxtke where phractxtke_id in (
select phractxtke_id from phractxtke
EXCEPT select entr_dst_id from entr_sdschap_phractxtke
EXCEPT select entr_dst_id from entr_phrac_phractxtke
EXCEPT select entr_dst_id from entr_plcsys_phractxtke
EXCEPT select entr_dst_id from entr_sdselt_phractxtke
)
UNION ALL
SELECT 'phractxt' AS entityname, COUNT(1) AS orphans from phractxt where phractxt_id in (
select phractxt_id from phractxt EXCEPT select entr_dst_id from entr_phractxtke_phractxt
)
UNION ALL
SELECT 'txt' AS entityname, COUNT(1) AS orphans  from txt where txt_id not in (
select entr_dst_id from entr_txtke_txt
)
UNION ALL
SELECT 'people' AS entityname, COUNT(1) AS orphans FROM people WHERE people_id IN (
SELECT people_id FROM people
EXCEPT select entr_dst_id from entr_dir_people
EXCEPT select entr_dst_id from entr_site_people
EXCEPT select entr_dst_id from entr_presence_people
)
UNION ALL
SELECT 'hrframework' AS entityname, COUNT(1) AS orphans from hrframework where hrframework_id not in (
select entr_dst_id from entr_people_hrframework
)
UNION ALL
SELECT 'plc' AS entityname, COUNT(1) AS orphans from plc where plc_id in (
select plc_id from plc
EXCEPT select entr_dst_id from entr_fiche_plc
EXCEPT select entr_dst_id from entr_substances_plc
EXCEPT select entr_dst_id from entr_nomclit_plc
)
UNION ALL
SELECT 'ext' AS entityname, COUNT(1) AS orphans from ext where ext_id in (
select endpt_id from endpt
EXCEPT select extr_dst_id from extr_fiche
EXCEPT select extr_dst_id from extr_presence
EXCEPT select extr_dst_id from extr_substances
EXCEPT select extr_dst_id from extr_sdsdoc
EXCEPT select extr_dst_id from extr_wftask
) AND ext.tp <>'TEMPLATE'
UNION ALL
SELECT 'endpt' AS entityname, COUNT(1) AS orphans from endpt where endpt_id in (
select endpt_id from endpt
EXCEPT select entr_dst_id from entr_fiche_endpt
EXCEPT select entr_dst_id from entr_study_endpt
EXCEPT select entr_dst_id from entr_substances_endpt
EXCEPT select entr_dst_id from entr_profile_endpt
)
UNION ALL
SELECT 'doc' AS entityname, COUNT(1) AS orphans from doc where doc_id in (
select doc_id from doc
EXCEPT select docr_dst_id from docr_dir
EXCEPT select docr_dst_id from docr_doc
EXCEPT select docr_dst_id from docr_sds
EXCEPT select docr_dst_id from docr_fiche
EXCEPT select docr_dst_id from docr_people
EXCEPT select docr_dst_id from docr_phractxtke
EXCEPT select docr_dst_id from docr_site
EXCEPT select docr_dst_id from docr_study
EXCEPT select docr_dst_id from docr_substances
EXCEPT select docr_dst_id from docr_part
EXCEPT select docr_dst_id from docr_phractxtke
EXCEPT select docr_dst_id from docr_profile
EXCEPT select entr_src_id from entr_doc_msg
EXCEPT select entr_dst_id from entr_ns_doc
) AND coalesce(tp,'') NOT IN ('TEMPLATE', 'mxgraph', 'svg')
