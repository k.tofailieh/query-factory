SELECT
COALESCE(suppliersList.dir_id, supplierFromFiche.dir_id) as dir_id,
COALESCE(suppliersList.displayname, supplierFromFiche.displayname, '') AS displayname,
COALESCE(suppliersList.givenname, supplierFromFiche.givenname, '') AS givenname,
COALESCE(suppliersList.surname, supplierFromFiche.surname, '') AS surname,
COALESCE(suppliersList.email, supplierFromFiche.email, '') AS email,
COALESCE(suppliersList.mobile, supplierFromFiche.mobile, '') AS mobile,
COALESCE(suppliersList.officephone, supplierFromFiche.officephone, '') AS officephone,
COALESCE(suppliersList.postaladdress, supplierFromFiche.postaladdress, '') AS postaladdress,
COALESCE(suppliersList.postalcode, supplierFromFiche.postalcode, '') AS postalcode,
COALESCE(suppliersList.city, supplierFromFiche.city, '') AS city,
COALESCE(suppliersList.country, supplierFromFiche.country, '') AS country,
COALESCE(suppliersList.fax, supplierFromFiche.fax, '') AS fax,
COALESCE(suppliersList.website, supplierFromFiche.website, '') AS website,
COALESCE(suppliersList.siret, supplierFromFiche.siret, '') AS siret,
COALESCE(suppliersList.nationality, supplierFromFiche.nationality, '') AS nationality,
 COALESCE(supplierFromFiche.nbproduct, 0) AS nbproduct,
 supplierCode AS suppliercode
FROM
(
SELECT supplier.dir_id, supplier.dn, supplier.cn, supplier.displayname, supplier.givenname, supplier.surname, supplier.email, supplier.mobile, entr_char1 as supplierCode, supplier.officephone, supplier.postaladdress, supplier.postalcode, supplier.city, supplier.country, supplier.fax, supplier.website, supplier.siret, supplier.nationality
FROM dir client
    INNER JOIN ENTR_dir_dir ENTR_dir_dir_SUPPLIER ON ENTR_dir_dir_SUPPLIER.entr_src_id = client.dir_id and ENTR_dir_dir_SUPPLIER.entr_tp in ('SUPPLIER','MANUFACTURER')
 INNER JOIN dir supplier on supplier.dir_id=ENTR_dir_dir_SUPPLIER.entr_dst_id
 WHERE client.dn LIKE CONCAT('o=', :user_organisation, ',%')
) as suppliersList
FULL OUTER JOIN
(
SELECT supplier.dir_id, supplier.dn, supplier.cn, supplier.displayname, supplier.givenname, supplier.surname, supplier.email, supplier.mobile, supplier.officephone, supplier.postaladdress, supplier.postalcode, supplier.city, supplier.country, supplier.fax, supplier.website, supplier.siret, supplier.nationality, count(1) as nbproduct
FROM
fiche fiche
    INNER JOIN ENTR_fiche_dir ENTR_fiche_dir_SUPPLIER ON ENTR_fiche_dir_SUPPLIER.entr_src_id = fiche.m_id and ENTR_fiche_dir_SUPPLIER.entr_tp in ('SUPPLIER','MANUFACTURER')
INNER JOIN dir supplier on supplier.dir_id=ENTR_fiche_dir_SUPPLIER.entr_dst_id
WHERE fiche.ns = :user_organisation
AND fiche.active > 0
GROUP by
supplier.dir_id, supplier.dn, supplier.cn, supplier.displayname, supplier.givenname, supplier.surname, supplier.email, supplier.mobile, supplier.officephone, supplier.postaladdress, supplier.postalcode, supplier.city, supplier.country, supplier.fax, supplier.website, supplier.siret, supplier.nationality
) as supplierFromFiche
ON supplierFromFiche.dir_id = suppliersList.dir_id ORDER BY UPPER(COALESCE(suppliersList.displayname, supplierFromFiche.displayname, '')) ASC;
