----------------------------------------------- META 1 --------------------------------------------------


SELECT m_ref as nom_produit, naml as nom_substance, substances.cas, substances.ec, nomclit.conc_real_ubound as concentration, plcsys_ke as mention_danger
FROM fiche
INNER JOIN entr_fiche_nomclit efn ON efn.entr_src_id = fiche.m_id AND efn.entr_tp = 'REAL_COMPO_SUBSTANCE'
INNER JOIN nomclit ON nomclit.nomclit_id = efn.entr_dst_id
INNER JOIN entr_nomclit_substances ens ON ens.entr_src_id = nomclit.nomclit_id AND ens.entr_tp = 'NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
INNER JOIN substances on ens.entr_dst_id = substances.sub_id
INNER JOIN entr_substances_plc AS esp ON esp.entr_src_id = sub_id AND esp.entr_tp = CONCAT('CLP_H_', :REGULATORY_REFERENTIAL)
INNER JOIN plc on plc.plc_id = esp.entr_dst_id
INNER JOIN entr_plc_plcsys as epp on epp.entr_src_id = plc_id
INNER JOIN plcsys on epp.entr_dst_id = plcsys_id
WHERE coalesce(nullif(fiche.ns, ''), '*') = :ORG
AND fiche.active =1
ORDER BY nom_produit ASC

------------------------------------------------- META 2---------------------------------------------------------------


SELECT DISTINCT m_ref as nom_produit, plcsys_ke as mention_danger, esp.entr_tp
from fiche
INNER JOIN entr_fiche_plc AS esp ON esp.entr_src_id = m_id
inner join plc on plc.plc_id = esp.entr_dst_id
INNER JOIN der_plc ON der_plc.der_src_id = plc.plc_id AND der_tp = 'VALID_FOR_REGULATORY'
INNER JOIN de as de1 ON de1.de_id = der_plc.der_dst_id AND de1.de_ke = :REGULATORY_REFERENTIAL
INNER JOIN entr_plc_plcsys as epp on epp.entr_src_id = plc_id
INNER JOIN plcsys on epp.entr_dst_id = plcsys_id
where coalesce(nullif(fiche.ns, ''), '*') = :ORG
and fiche.active =1
order by nom_produit asc;

------------------------------------------------- META 3 ----------------------------------------------------------

select m_ref, site_li
from fiche
INNER JOIN entr_presence_fiche AS epf ON epf.entr_dst_id = m_id
LEFT OUTER JOIN entr_site_presence AS esp ON esp.entr_dst_id = epf.entr_src_id
LEFT OUTER JOIN site ON site_id = esp.entr_src_id
WHERE tree_key like :ORG
and coalesce(nullif(fiche.ns, ''), '*') = :ORG
and fiche.active =1




