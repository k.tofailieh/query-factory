WITH
FicheHClassCat as (
SELECT m_id ,epps.entr_dst_id as hclasscat_id, fiche.reference, fiche.marguage
FROM fiche
INNER JOIN entr_fiche_plc efp on efp.entr_src_id=fiche.m_id AND efp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
INNER JOIN entr_plc_plcsys epps ON efp.entr_dst_id = epps.entr_src_id AND epps.entr_tp IN ('HAZARD_CLASS_CAT')
WHERE COALESCE(fiche.active,0) <> 0 AND coalesce(nullif(fiche.ns, ''), '*') = :user_organisation
)
, HClassCat as (
SELECT DISTINCT hclasscat_id from FicheHClassCat
)
, HPGS as (
SELECT hclasscat_id , pls1.name as name, txt.lg as lg, CONCAT(pls1.plcsys_ke , ' : ' , txt.txt) as phrase
FROM HClassCat
INNER JOIN entr_plcsys_plcsys epsps ON epsps.entr_src_id = hclasscat_id AND epsps.entr_tp in (CONCAT('GHS_H_STMT_',:REGULATORY_REFERENTIAL),CONCAT('PSTMT_',:REGULATORY_REFERENTIAL))
INNER JOIN plcsys pls1 ON pls1.plcsys_id = epsps.entr_dst_id
INNER JOIN txtke ON txtke.ke = concat(pls1.plcsys_ke, '-li')
INNER JOIN entr_txtke_txt ON entr_txtke_txt.entr_src_id = txtke_id
INNER JOIN txt  ON entr_txtke_txt.entr_dst_id = txt.txt_id AND txt.lg in ('en', 'fr')
UNION ALL
SELECT hclasscat_id , pls1.name, null as lg, pls1.plcsys_ke as phrase
FROM HClassCat
INNER JOIN entr_plcsys_plcsys epsps ON epsps.entr_src_id = hclasscat_id AND epsps.entr_tp in (CONCAT('GHS_PICTO_',:REGULATORY_REFERENTIAL),CONCAT('SIGNALWORD_',:REGULATORY_REFERENTIAL))
INNER JOIN plcsys pls1 ON pls1.plcsys_id = epsps.entr_dst_id
)
select
m_id, reference m_ref, marguage
, string_agg( DISTINCT phrase   , ';' ORDER BY phrase) FILTER (WHERE name = 'GHS_H_STMT' AND lg ='en')   AS clph_en
, string_agg( DISTINCT phrase   , ';' ORDER BY phrase) FILTER (WHERE name = 'GHS_H_STMT' AND lg ='fr')   AS clph_fr
, string_agg( DISTINCT phrase   , ';' ORDER BY phrase) FILTER (WHERE name = 'P_STMT' AND lg ='en')   AS clpp_en
, string_agg( DISTINCT phrase   , ';' ORDER BY phrase) FILTER (WHERE name = 'P_STMT' AND lg ='fr')   AS clpp_fr
, min(phrase) FILTER (WHERE name = 'SIGNALWORD') as signalword
, string_agg( DISTINCT phrase   , ';' ORDER BY phrase) FILTER (WHERE name = 'GHS_PICTO')   AS picto
from FicheHClassCat
inner join HPGS using (hclasscat_id)
GROUP BY  reference, marguage,  m_id;









--------------------------------------------

WITH
FicheHClassCat as (
SELECT m_id ,epps.entr_dst_id as hclasscat_id, fiche.reference, fiche.marguage
FROM fiche
INNER JOIN entr_fiche_plc efp on efp.entr_src_id=fiche.m_id AND efp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
INNER JOIN entr_plc_plcsys epps ON efp.entr_dst_id = epps.entr_src_id AND epps.entr_tp IN ('HAZARD_CLASS_CAT')
WHERE COALESCE(fiche.active,0) <> 0 AND split_part(fiche.ns,'/',1) = :user_organisation
)
, HClassCat as (
SELECT DISTINCT hclasscat_id from FicheHClassCat
)
, HPGS as (
SELECT hclasscat_id , pls1.name as name, txt.lg as lg, CONCAT(pls1.plcsys_ke , ' : ' , txt.txt) as phrase
FROM HClassCat
INNER JOIN entr_plcsys_plcsys epsps ON epsps.entr_src_id = hclasscat_id AND epsps.entr_tp in (CONCAT('GHS_H_STMT_',:REGULATORY_REFERENTIAL),CONCAT('PSTMT_',:REGULATORY_REFERENTIAL))
INNER JOIN plcsys pls1 ON pls1.plcsys_id = epsps.entr_dst_id
INNER JOIN txtke ON txtke.ke = concat(pls1.plcsys_ke, '-li')
INNER JOIN entr_txtke_txt ON entr_txtke_txt.entr_src_id = txtke_id
INNER JOIN txt  ON entr_txtke_txt.entr_dst_id = txt.txt_id AND txt.lg in ('en', 'fr')
UNION ALL
SELECT hclasscat_id , pls1.name, null as lg, pls1.plcsys_ke as phrase
FROM HClassCat
INNER JOIN entr_plcsys_plcsys epsps ON epsps.entr_src_id = hclasscat_id AND epsps.entr_tp in (CONCAT('GHS_PICTO_',:REGULATORY_REFERENTIAL),CONCAT('SIGNALWORD_',:REGULATORY_REFERENTIAL))
INNER JOIN plcsys pls1 ON pls1.plcsys_id = epsps.entr_dst_id
)
select
m_id, reference m_ref, marguage
, string_agg( DISTINCT phrase   , ';' ORDER BY phrase) FILTER (WHERE name = 'GHS_H_STMT' AND lg ='en')   AS clph_en
, string_agg( DISTINCT phrase   , ';' ORDER BY phrase) FILTER (WHERE name = 'GHS_H_STMT' AND lg ='fr')   AS clph_fr
, string_agg( DISTINCT phrase   , ';' ORDER BY phrase) FILTER (WHERE name = 'P_STMT' AND lg ='en')   AS clpp_en
, string_agg( DISTINCT phrase   , ';' ORDER BY phrase) FILTER (WHERE name = 'P_STMT' AND lg ='fr')   AS clpp_fr
, min(phrase) FILTER (WHERE name = 'SIGNALWORD') as signalword
, string_agg( DISTINCT phrase   , ';' ORDER BY phrase) FILTER (WHERE name = 'GHS_PICTO')   AS picto
from FicheHClassCat
inner join HPGS using (hclasscat_id)
GROUP BY  reference, marguage,  m_id

