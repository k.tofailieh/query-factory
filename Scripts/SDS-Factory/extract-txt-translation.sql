with ketxt
as (
select txt.*, bundle, ke from txt inner join txtke on exists (select 1 from entr_txtke_txt where entr_src_id = txtke_id and entr_dst_id = txt_id) and lg in ('en','fr',:lang) and bundle not in ('hazard-class-cat','bo', 'eco-workrisk','ac','erc','su','proc','pc') and bundle not like 'CHESAR%' and bundle not like '%CUSTOM_OPTIONS' and bundle not like '%OLD%'
/* select * from txt inner join entr_txtke_txt on entr_dst_id = txt_id inner join txtke on entr_src_id = txtke_id */
),
ketxten as (
select * from ketxt where lg = 'en'
),
ketxtfr as (
select * from ketxt where lg = 'fr'
),
ketxtenfr as (
select * from ketxtfr where (bundle, ke) not in (select bundle, ke from ketxten)
union all
select * from ketxten
),
ketxtru as (
select * from ketxt where lg = :lang
)
select
txtke.txtke_id as ke_id, txtke.bundle as bundle, txtke.ke as ke,
ketxtenfr.txt_id as src_txt_id,
ketxtenfr.lg as src_lg,
ketxtenfr.ns as src_ns,
ketxtenfr.txt as src_txt,
trim(replace(translate(upper(ecoft_unaccent(trim(ketxtenfr.txt))),
$$,;:/()[]"'&{}-+_.$*%\/<>0123456789?$$,
$$###################################$$),'#','')) as upperTXT,
ketxtenfr.author as src_autor,
ketxtenfr.md_dt as src_date,
ketxtenfr.nt as src_note,
ketxtenfr.tech_info as src_info,
ketxtenfr.quality as src_quality,
ketxtru.txt_id as txt_id,
ketxtru.lg as lg,
ketxtru.ns as ns,
ketxtru.txt as txt,
ketxtru.author as autor,
ketxtru.md_dt as md_dt,
ketxtru.reviewer as reviewer,
ketxtru.co as co,
ketxtru.nt as nt,
ketxtru.tech_info as tech_info,
ketxtru.tech_code as code,
ketxtru.quality as quality
from txtke
inner join ketxtenfr using (bundle, ke)
left outer join ketxtru using (bundle, ke)
order by upperTXT, ketxtenfr.txt, ketxtenfr.lg, ketxtenfr.quality, ketxtru.quality, ketxtru.txt, txtke.bundle, txtke.ke
