SELECT m_id, m_ref as reference, marguage as nom_produit,
    /* Fournisseur */
    SUPPLIER.displayname as supplier_displayname,
    /* Fabriquant */
    MANUFACTURER.displayname as manufacturer_displayname,
    /* Famille */
    COALESCE(FAMILY_TRANSLATION.txt, FAMILY.ke) as famille,
    COALESCE(SUB_FAMILY_TRANSLATION.txt, SUB_FAMILY.ke) as sous_famille,
    /* Kits */
    /* Fonction tech */
    /* Utilisation spécifique */
    MATERIAL_SUPPLEMENTAL_DATA1_FORM_FIELD_VALUE_KitName.val_char as kitname,MATERIAL_SUPPLEMENTAL_DATA1_FORM_FIELD_VALUE_UtilisationSpecifique.val_char as UtilisationSpecifique,MATERIAL_SUPPLEMENTAL_DATA1_FORM_FIELD_VALUE_FonctionTechnique.val_char as FonctionTechnique,
     /* Sites utilisation*/
     site_li as site, presence.presence_dt1 as debut, presence.presence_dt2 as fin,
     /* Qte */
     amountOfMaterial.val_num as amountOfMaterial, amountOfMaterial_Unit.val_char as amountOfMaterial_Unit, amountOfMaterial_UnitPerFreq.val_char as amountOfMaterial_UnitPerFreq,
    'last select' as lastcol
FROM fiche
/* Fournisseur */
LEFT OUTER JOIN entr_fiche_dir lSUPPLIER ON lSUPPLIER.entr_src_id = m_id AND lSUPPLIER.entr_tp = 'SUPPLIER' LEFT OUTER JOIN dir SUPPLIER on supplier.dir_id = lSUPPLIER.entr_dst_id
/* Fabriquant */
LEFT OUTER JOIN entr_fiche_dir lMANUFACTURER ON lMANUFACTURER.entr_src_id = m_id AND lMANUFACTURER.entr_tp = 'MANUFACTURER' LEFT OUTER JOIN dir MANUFACTURER on MANUFACTURER.dir_id = lMANUFACTURER.entr_dst_id
/* Famille */
LEFT OUTER JOIN entr_fiche_lov lFAMILY ON lFAMILY.entr_src_id = m_id AND lFAMILY.entr_tp = 'FAMILY' LEFT OUTER JOIN lov FAMILY on FAMILY.lov_id = lFAMILY.entr_dst_id and FAMILY.tp = 'FAMILY'
  LEFT OUTER JOIN
    (SELECT * FROM txtke INNER JOIN entr_txtke_txt ON entr_src_id = txtke_id AND entr_tp = 'TRANSLATION' INNER JOIN txt on txt_id = entr_dst_id and txt.ns = 'GrtGaz'  AND lg like 'fr' AND txt_id > 1E8  where txtke.ns = 'GrtGaz%' and bundle like 'LOV\_' and txtke_id > 1E8) FAMILY_TRANSLATION
    ON FAMILY_TRANSLATION.bundle LIKE 'LOV\_' || FAMILY.tp || '\_' || 'GrtGaz' AND FAMILY_TRANSLATION.ke LIKE 'LOV\_' || FAMILY.ke || '\_' || 'GrtGaz'
LEFT OUTER JOIN entr_fiche_lov lSUB_FAMILY ON lSUB_FAMILY.entr_src_id = m_id AND lSUB_FAMILY.entr_tp = 'SUB_FAMILY' LEFT OUTER JOIN lov SUB_FAMILY on SUB_FAMILY.lov_id = lSUB_FAMILY.entr_dst_id and SUB_FAMILY.tp = 'SUBFAMILY'
  LEFT OUTER JOIN
    (SELECT * FROM txtke INNER JOIN entr_txtke_txt ON entr_src_id = txtke_id AND entr_tp = 'TRANSLATION' INNER JOIN txt on txt_id = entr_dst_id and txt.ns ='GrtGaz'  AND lg like 'fr' AND txt_id > 1E8  where txtke.ns = 'GrtGaz' and bundle like 'LOV\_' and txtke_id > 1E8) SUB_FAMILY_TRANSLATION
    ON SUB_FAMILY_TRANSLATION.bundle LIKE 'LOV\_' || SUB_FAMILY.tp || '\_' || 'GrtGaz' AND SUB_FAMILY_TRANSLATION.ke LIKE 'LOV\_' || SUB_FAMILY.ke || '\_' || 'GrtGaz'
/* Kits */
/* Fonction tech */
/* Utilisation spécifique */
LEFT OUTER JOIN extr_fiche lMATERIAL_SUPPLEMENTAL_DATA1 ON lMATERIAL_SUPPLEMENTAL_DATA1.extr_src_id = m_id AND lMATERIAL_SUPPLEMENTAL_DATA1.extr_tp = 'FORM_INSTANCE'
LEFT OUTER JOIN
    (SELECT *
    FROM ext MATERIAL_SUPPLEMENTAL_DATA1
    INNER JOIN entr_ext_extfld lFORM_FIELD_VALUE_KitName on entr_src_id = ext_id AND entr_tp = 'FORM_FIELD_VALUE'
    INNER JOIN extfld FORM_FIELD_VALUE_KitName on FORM_FIELD_VALUE_KitName.extfld_id = lFORM_FIELD_VALUE_KitName.entr_dst_id and itemname = 'KitName'
    WHERE MATERIAL_SUPPLEMENTAL_DATA1.ke = 'MATERIAL_SUPPLEMENTAL_DATA1' AND MATERIAL_SUPPLEMENTAL_DATA1.tp = 'FORM_INSTANCE' AND MATERIAL_SUPPLEMENTAL_DATA1.ext_data_format = 'JSON.EXT4+ECO2'
    ) MATERIAL_SUPPLEMENTAL_DATA1_FORM_FIELD_VALUE_KitName
    ON MATERIAL_SUPPLEMENTAL_DATA1_FORM_FIELD_VALUE_KitName.ext_id = lMATERIAL_SUPPLEMENTAL_DATA1.extr_dst_id
LEFT OUTER JOIN
    (SELECT *
    FROM ext MATERIAL_SUPPLEMENTAL_DATA1
    INNER JOIN entr_ext_extfld lFORM_FIELD_VALUE_UtilisationSpecifique on entr_src_id = ext_id AND entr_tp = 'FORM_FIELD_VALUE'
    INNER JOIN extfld FORM_FIELD_VALUE_UtilisationSpecifique on FORM_FIELD_VALUE_UtilisationSpecifique.extfld_id = lFORM_FIELD_VALUE_UtilisationSpecifique.entr_dst_id and itemname = 'UtilisationSpecifique'
    WHERE MATERIAL_SUPPLEMENTAL_DATA1.ke = 'MATERIAL_SUPPLEMENTAL_DATA1' AND MATERIAL_SUPPLEMENTAL_DATA1.tp = 'FORM_INSTANCE' AND MATERIAL_SUPPLEMENTAL_DATA1.ext_data_format = 'JSON.EXT4+ECO2'
    ) MATERIAL_SUPPLEMENTAL_DATA1_FORM_FIELD_VALUE_UtilisationSpecifique
    ON MATERIAL_SUPPLEMENTAL_DATA1_FORM_FIELD_VALUE_UtilisationSpecifique.ext_id = lMATERIAL_SUPPLEMENTAL_DATA1.extr_dst_id
LEFT OUTER JOIN
    (SELECT *
    FROM ext MATERIAL_SUPPLEMENTAL_DATA1
    INNER JOIN entr_ext_extfld lFORM_FIELD_VALUE_FonctionTechnique on entr_src_id = ext_id AND entr_tp = 'FORM_FIELD_VALUE'
    INNER JOIN extfld FORM_FIELD_VALUE_FonctionTechnique on FORM_FIELD_VALUE_FonctionTechnique.extfld_id = lFORM_FIELD_VALUE_FonctionTechnique.entr_dst_id and itemname = 'FonctionTechnique'
    WHERE MATERIAL_SUPPLEMENTAL_DATA1.ke = 'MATERIAL_SUPPLEMENTAL_DATA1' AND MATERIAL_SUPPLEMENTAL_DATA1.tp = 'FORM_INSTANCE' AND MATERIAL_SUPPLEMENTAL_DATA1.ext_data_format = 'JSON.EXT4+ECO2'
    ) MATERIAL_SUPPLEMENTAL_DATA1_FORM_FIELD_VALUE_FonctionTechnique
    ON MATERIAL_SUPPLEMENTAL_DATA1_FORM_FIELD_VALUE_FonctionTechnique.ext_id = lMATERIAL_SUPPLEMENTAL_DATA1.extr_dst_id
/* Sites utilisation*/
INNER JOIN entr_presence_fiche ON entr_presence_fiche.entr_dst_id = m_id AND entr_presence_fiche.entr_tp = 'MATERIAL'
INNER JOIN presence ON presence_id = entr_presence_fiche.entr_src_id
INNER JOIN entr_site_presence ON entr_site_presence.entr_dst_id = presence_id AND entr_site_presence.entr_tp = 'MATERIAL'
INNER JOIN site on site_id = entr_site_presence.entr_src_id
/* Qte */
LEFT OUTER JOIN
    (
    SELECT *
    FROM extfld
    INNER JOIN entr_ext_extfld on entr_ext_extfld.entr_dst_id = extfld_id and entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
    INNER JOIN extr_presence on extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id and extr_presence.extr_tp = 'FORM_INSTANCE'
    WHERE extfld.itemname = 'amountOfMaterial' AND extfld.ns = 'GrtGaz'
    ) amountOfMaterial on amountOfMaterial.extr_src_id = presence_id
LEFT OUTER JOIN
    (
    SELECT *
    FROM extfld
    INNER JOIN entr_ext_extfld on entr_ext_extfld.entr_dst_id = extfld_id and entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
    INNER JOIN extr_presence on extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id and extr_presence.extr_tp = 'FORM_INSTANCE'
    WHERE extfld.itemname = 'amountOfMaterial_Unit' AND extfld.ns = 'GrtGaz'
    ) amountOfMaterial_Unit on amountOfMaterial_Unit.extr_src_id = presence_id
LEFT OUTER JOIN
    (
    SELECT *
    FROM extfld
    INNER JOIN entr_ext_extfld on entr_ext_extfld.entr_dst_id = extfld_id and entr_ext_extfld.entr_tp = 'FORM_FIELD_VALUE'
    INNER JOIN extr_presence on extr_presence.extr_dst_id = entr_ext_extfld.entr_src_id and extr_presence.extr_tp = 'FORM_INSTANCE'
    WHERE extfld.itemname = 'amountOfMaterial_UnitPerFreq' AND extfld.ns = 'GrtGaz' || '%'
    ) amountOfMaterial_UnitPerFreq on amountOfMaterial_UnitPerFreq.extr_src_id = presence_id
WHERE COALESCE(fiche.active,0) <> 0 AND fiche.ns = 'GrtGaz' and m_id in (:mIds)
