------------------------------------------------------------ META 1 ---------------------------------------------------
WITH setSelected as (
  select inforeqset_id from inforeqset
  where inforeqset_cmt ilike :user_organisation
  and inforeqset_st = 'ACTIVE'
), suppliers as (
  select supplier.dir_id, supplier.cn, supplier.displayname, entr_char1 from dir org
  inner join entr_dir_dir on org.dir_id = entr_src_id and entr_tp = 'SUPPLIER'
  inner join dir supplier on entr_dst_id = supplier.dir_id
  where org.cn = :user_organisation
)
select part.part_id as partId,
  part.name_1 as articleName, part.reference_1 as articleCode,
  part.name_2 as supplierArticleName, part.reference_2 as supplierArticleCode, inforeq_id,
  d1.dir_id as supplierId, d1.cn as supplierAccount, d1.displayname as supplierName, d1.entr_char1 as supplierCode,
  d3.dir_id as contactId, d3.dn as groupId, d3.cn as contactAccount, d3.displayname as contactName, d3.email,
  d4.dir_id as organizationId, d4.cn as organizationAccount, d4.displayname as organizationName
from inforeqset
inner join entr_inforeqset_inforeq eii on eii.entr_src_id = inforeqset_id
inner join inforeq on inforeq_id = eii.entr_dst_id and inforeq_st = 'VALID'
inner join entr_inforeq_part eip on eip.entr_src_id = inforeq_id
inner join part on part_id = eip.entr_dst_id
inner join entr_part_dir epd on epd.entr_src_id = part_id
inner join suppliers d1 on d1.dir_id = epd.entr_dst_id
inner join entr_inforeq_dir eid on eid.entr_src_id = inforeq_id and eid.entr_tp = 'ASKED'
inner join dir d3 on d3.dir_id = eid.entr_dst_id
left join entr_dir_dir edd2 on edd2.entr_dst_id = d3.dir_id and edd2.entr_tp = 'MEMBER'
left join dir d4 on d4.dir_id = edd2.entr_src_id
where inforeqset_id = :inforeqset_id and inforeqset_id in (select inforeqset_id from setSelected) and part.ns = :user_organisation
order by part.name_1;



--------------------------------------------------------- META 2 -------------------------------------------------------
WITH setSelected as (
  select inforeqset_id from inforeqset
  where inforeqset_cmt ilike :user_organisation
  and inforeqset_st = 'INACTIVE'
), suppliers as (
  select supplier.dir_id, supplier.cn, supplier.displayname, entr_char1 from dir org
  inner join entr_dir_dir on org.dir_id = entr_src_id and entr_tp = 'SUPPLIER'
  inner join dir supplier on entr_dst_id = supplier.dir_id
  where org.cn = :user_organisation
)
select part.part_id as partId,
  part.name_1 as articleName, part.reference_1 as articleCode,
  part.name_2 as supplierArticleName, part.reference_2 as supplierArticleCode, inforeq_id,
  d1.dir_id as supplierId, d1.cn as supplierAccount, d1.displayname as supplierName, d1.entr_char1 as supplierCode,
  d3.dir_id as contactId, d3.dn as groupId, d3.cn as contactAccount, d3.displayname as contactName, d3.email,
  d4.dir_id as organizationId, d4.cn as organizationAccount, d4.displayname as organizationName
from inforeqset
inner join entr_inforeqset_inforeq eii on eii.entr_src_id = inforeqset_id
inner join inforeq on inforeq_id = eii.entr_dst_id and inforeq_st = 'VALID'
inner join entr_inforeq_part eip on eip.entr_src_id = inforeq_id
inner join part on part_id = eip.entr_dst_id
inner join entr_part_dir epd on epd.entr_src_id = part_id
inner join suppliers d1 on d1.dir_id = epd.entr_dst_id
inner join entr_inforeq_dir eid on eid.entr_src_id = inforeq_id and eid.entr_tp = 'ASKED'
inner join dir d3 on d3.dir_id = eid.entr_dst_id
left join entr_dir_dir edd2 on edd2.entr_dst_id = d3.dir_id and edd2.entr_tp = 'MEMBER'
left join dir d4 on d4.dir_id = edd2.entr_src_id
where inforeqset_id = :inforeqset_id and inforeqset_id  in (select inforeqset_id from setSelected)
order by part.name_1;

------------------------------------------------------ META 3 ----------------------------------------------------------
WITH setSelected as (
  select inforeqset_id from inforeqset
  where inforeqset_cmt ilike :user_organisation
  and inforeqset_st = 'ACTIVE'
), suppliers as (
  select supplier.dir_id, supplier.cn, supplier.displayname, entr_char1 from dir org
  inner join entr_dir_dir on org.dir_id = entr_src_id and entr_tp = 'SUPPLIER'
  inner join dir supplier on entr_dst_id = supplier.dir_id
  where org.cn = :user_organisation
)
select p2.part_id as nomclitId, p2.name_1 as nomclitName, p2.reference_1 as nomclitCode,
  p1.part_id as artcileId, p1.name_1 as artcileName, p1.reference_1 as artcileCode,
  d1.dir_id as fournisseurId, d1.displayname as fournisseurName, d1.entr_char1 as fournisseurCode,
  d3.dir_id as contactId, d3.dn as groupId, d3.displayname as contactName, d3.email
from inforeqset
inner join entr_inforeqset_inforeq eii on eii.entr_src_id = inforeqset_id
inner join inforeq on inforeq_id = eii.entr_dst_id and inforeq_st = 'VALID'
inner join entr_inforeq_part eip on eip.entr_src_id = inforeq_id
inner join part p1 on p1.part_id = eip.entr_dst_id
inner join entr_nomclit_part enp on enp.entr_dst_id = p1.part_id and enp.entr_tp  = 'NOMENCLATURE_ITEM_FOR_PART_IS'
inner join nomclit on nomclit_id = enp.entr_src_id and st = 'VALID'
inner join entr_part_nomclit epn on epn.entr_dst_id = nomclit_id and epn.entr_tp = 'CONTAINS'
inner join part p2 on p2.part_id = epn.entr_src_id
inner join entr_part_dir epd on epd.entr_src_id = p1.part_id
inner join suppliers d1 on d1.dir_id = epd.entr_dst_id
inner join entr_inforeq_dir eid on eid.entr_src_id = inforeq_id and eid.entr_tp = 'ASKED'
inner join dir d3 on d3.dir_id = eid.entr_dst_id
where inforeqset_id in (select inforeqset_id from setSelected)
order by p2.name_1,p1.name_1;

