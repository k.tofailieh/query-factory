----------------------------------------------- META 1 -----------------------------------------------------------

/* COSMETICs BOMs */
SELECT coalesce(nullif(prod.ns, ''), '*') AS org, prod.marguage AS product_code, prod.m_ref AS product_name,
    prodnomenclature.conc_real_ubound AS rawmaterial_concentration, rawmatarial.marguage AS rawmaterial_code, rawmatarial.m_ref AS rawmaterial_name,
    ingredient.tp AS ingredient_type, ENTR_fiche_nomclit_DIRECT_COMPO.entr_char1 as origin, rawmatnomenclature.conc_real_ubound AS ingredient_concentration, inci.inciname, inci.cas,
    prodingredient.conc_real_ubound AS ingredient_total_product_concentration,
    ingredientfunction.cosmetic_ingredient_function_eu, ingredientfunction.cosmetic_ingredient_function_usa
FROM fiche prod
/* Get down the nomenclature from prod to INCI*/
    INNER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_DIRECT_COMPO_RM ON ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_src_id = prod.m_id and ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_tp='DIRECT_COMPO'
INNER JOIN nomclit prodnomenclature on prodnomenclature.nomclit_id=ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_dst_id
    INNER JOIN ENTR_nomclit_fiche ENTR_nomclit_fiche_HAS_RM ON ENTR_nomclit_fiche_HAS_RM.entr_src_id = prodnomenclature.nomclit_id and ENTR_nomclit_fiche_HAS_RM.entr_tp='HAS'
INNER JOIN fiche rawmatarial on rawmatarial.m_id=ENTR_nomclit_fiche_HAS_RM.entr_dst_id
    INNER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_DIRECT_COMPO ON ENTR_fiche_nomclit_DIRECT_COMPO.entr_src_id = rawmatarial.m_id and ENTR_fiche_nomclit_DIRECT_COMPO.entr_tp='DIRECT_COMPO'
INNER JOIN nomclit rawmatnomenclature on rawmatnomenclature.nomclit_id=ENTR_fiche_nomclit_DIRECT_COMPO.entr_dst_id
    INNER JOIN ENTR_nomclit_fiche ENTR_nomclit_fiche_HAS ON ENTR_nomclit_fiche_HAS.entr_src_id = rawmatnomenclature.nomclit_id and ENTR_nomclit_fiche_HAS.entr_tp='HAS'
INNER JOIN fiche ingredient on ingredient.m_id=ENTR_nomclit_fiche_HAS.entr_dst_id
    INNER JOIN anx.ENTR_fiche_inci ENTR_fiche_inci_INCI ON ENTR_fiche_inci_INCI.entr_src_id = ingredient.m_id AND ENTR_fiche_inci_INCI.entr_tp='INGREDIENT'
INNER JOIN anx.inci inci on inci.inci_id=ENTR_fiche_inci_INCI.entr_dst_id
/* Get the flatten and grouped concentrations. Attention. Can be different from the simple multiplication because an ingredient can, be in many raw materials */
    INNER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_REAL_COMPO ON ENTR_fiche_nomclit_REAL_COMPO.entr_src_id = prod.m_id and ENTR_fiche_nomclit_REAL_COMPO.entr_tp='REAL_COMPO'
INNER JOIN nomclit prodingredient on prodingredient.nomclit_id=ENTR_fiche_nomclit_REAL_COMPO.entr_dst_id
    INNER JOIN ENTR_nomclit_fiche ENTR_nomclit_fiche_HAS_REAL ON ENTR_nomclit_fiche_HAS_REAL.entr_src_id = prodingredient.nomclit_id and ENTR_nomclit_fiche_HAS_REAL.entr_dst_id = ingredient.m_id AND ENTR_nomclit_fiche_HAS.entr_tp='HAS'
/* Get the ingredient function */
LEFT OUTER JOIN
    (SELECT profile.tp, string_agg(app.app_ke,',') FILTER (WHERE app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_EU') AS COSMETIC_INGREDIENT_FUNCTION_EU, string_agg(app.app_ke,',') FILTER (WHERE app.app_tp = 'COSMETIC_INGREDIENT_FUNCTION_USA') AS COSMETIC_INGREDIENT_FUNCTION_USA, ENTR_nomclit_profile_COSMETIC_FUNCTION_APPLIANCE.entr_src_id, ENTR_profile_nomclit_COSMETIC_FUNCTION_SOURCE.entr_dst_id
     FROM ENTR_nomclit_profile ENTR_nomclit_profile_COSMETIC_FUNCTION_APPLIANCE
     INNER JOIN profile on profile.profile_id=ENTR_nomclit_profile_COSMETIC_FUNCTION_APPLIANCE.entr_dst_id
     INNER JOIN ENTR_profile_nomclit ENTR_profile_nomclit_COSMETIC_FUNCTION_SOURCE ON ENTR_profile_nomclit_COSMETIC_FUNCTION_SOURCE.entr_src_id = profile.profile_id AND ENTR_profile_nomclit_COSMETIC_FUNCTION_SOURCE.entr_tp='COSMETIC_FUNCTION_SOURCE'
        INNER JOIN ENTR_profile_app ENTR_profile_app_COSMETIC_FUNCTION_IDENTITY ON ENTR_profile_app_COSMETIC_FUNCTION_IDENTITY.entr_src_id = profile.profile_id AND ENTR_profile_app_COSMETIC_FUNCTION_IDENTITY.entr_tp='COSMETIC_FUNCTION_IDENTITY'
    INNER JOIN app app ON app.app_id=ENTR_profile_app_COSMETIC_FUNCTION_IDENTITY.entr_dst_id
     GROUP BY profile.tp, ENTR_nomclit_profile_COSMETIC_FUNCTION_APPLIANCE.entr_src_id, ENTR_profile_nomclit_COSMETIC_FUNCTION_SOURCE.entr_dst_id
     ) AS ingredientfunction on ingredientfunction.entr_src_id = prodnomenclature.nomclit_id AND ingredientfunction.entr_dst_id = rawmatnomenclature.nomclit_id

WHERE coalesce(nullif(prod.ns, ''), '*') = coalesce(nullif(:user_organisation, ''), '*')
ORDER BY
prod.marguage, prod.m_id, prodnomenclature.conc_real_ubound DESC,
rawmatarial.marguage, rawmatarial.m_id, rawmatnomenclature.conc_real_ubound DESC,
ingredient.marguage, ingredient.m_id;

----------------------------------------------- META 2 -----------------------------------------------------------

/* COSMETICs Direct BOMs */
SELECT coalesce(nullif(prod.ns, ''), '*') AS org, prod.marguage AS product_code, prod.m_ref AS product_name,
    prodnomenclature.conc_real_ubound AS rawmaterial_concentration, rawmaterial.marguage AS rawmaterial_code, rawmaterial.m_ref AS rawmaterial_name
FROM fiche prod
/* Get down the nomenclature from prod to INCI*/
    INNER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_DIRECT_COMPO_RM ON ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_src_id = prod.m_id and ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_tp='DIRECT_COMPO'
INNER JOIN nomclit prodnomenclature on prodnomenclature.nomclit_id=ENTR_fiche_nomclit_DIRECT_COMPO_RM.entr_dst_id
    INNER JOIN ENTR_nomclit_fiche ENTR_nomclit_fiche_HAS_RM ON ENTR_nomclit_fiche_HAS_RM.entr_src_id = prodnomenclature.nomclit_id and ENTR_nomclit_fiche_HAS_RM.entr_tp='HAS'
INNER JOIN fiche rawmaterial on rawmaterial.m_id=ENTR_nomclit_fiche_HAS_RM.entr_dst_id
WHERE coalesce(nullif(prod.ns, ''), '*') = coalesce(nullif(:user_organisation, ''), '*')
ORDER BY
prod.marguage, prod.m_id, prodnomenclature.conc_real_ubound DESC,
rawmaterial.marguage, rawmaterial.m_id;
