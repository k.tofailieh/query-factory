
----------------------------------------------- META 1 -----------------------------------------------------------
/* COSMETICs product */
SELECT coalesce(nullif(prod.ns, ''),'*') AS org, prod.m_id as entity_id, prod.tp as product_type, prod.marguage AS product_code, prod.m_ref AS product_name
FROM fiche prod
WHERE prod.tp in ('PRODUCT') and prod.active in (1,2)
ORDER BY
coalesce(prod.marguage, prod.m_ref), prod.m_id



----------------------------------------------- META 2 -----------------------------------------------------------


/* COSMETICs PF docs */
SELECT coalesce(nullif(prod.ns,''),'*') AS org, prod.m_id as entity_id, prod.tp as product_type, prod.marguage AS product_code, prod.m_ref AS product_name,
doc_file_name, doc_cr_dt, doc_tl AS title, md_dt AS document_md_dt, lg,DOC_TYPE.de_ke AS document_type,
DER_DOC_REVIEW_STATE.der_dt1 AS state_dt,
  '<' ||prod.m_id ||  '+' || DOC_TYPE.de_ke || '/>' AS doc_uid, /* 123456+PP_BOM */
DOC_REVIEW_STATE.de_ke AS review_state
FROM fiche prod
INNER JOIN docr_fiche ON docr_src_id = m_id AND docr_tp = 'ATTACHMENT'
INNER JOIN doc ON doc_id = docr_dst_id
INNER JOIN der_doc DER_DOC_TYPE ON DER_DOC_TYPE.der_src_id  = doc_id and DER_DOC_TYPE.der_tp = 'DOC_TYPE'
INNER JOIN de DOC_TYPE ON DOC_TYPE.de_id = DER_DOC_TYPE.der_dst_id
LEFT OUTER JOIN der_doc DER_DOC_REVIEW_STATE ON DER_DOC_REVIEW_STATE.der_src_id  = doc_id and DER_DOC_REVIEW_STATE.der_tp = 'COSMETIC_DOCUMENT_REVIEW_STATE'
LEFT OUTER JOIN de DOC_REVIEW_STATE ON DOC_REVIEW_STATE.de_id = DER_DOC_REVIEW_STATE.der_dst_id
WHERE prod.tp in ('PRODUCT') and prod.active in (1,2)
ORDER BY
coalesce(prod.marguage, prod.m_ref), prod.m_id
